// 1. Imports to config the connection to SQL SERVER
using EFCodeFirst.DataAccess;
using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);

// 2. Obtain Connection String from appSettings.json
const string CONNECTIONAME = "ContactsDB";
var connectionString = builder.Configuration.GetConnectionString(CONNECTIONAME);

// 3. Add DbContext to the services of app
// We tell that is a connection with SQL Server
builder.Services.AddDbContext<ContactsContext>(x => x.UseSqlServer(connectionString));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();