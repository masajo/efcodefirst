﻿using Microsoft.EntityFrameworkCore;
using EFCodeFirst.Models.DataModels;

namespace EFCodeFirst.DataAccess
{
    public class ContactsContext: DbContext
    {

        public ContactsContext(DbContextOptions<ContactsContext> options): base(options)
        {
            
        }

        // Define Sets of tables in order to create the Data Base
        public DbSet<Contact>? Contacts { get; set; }

    }
}
